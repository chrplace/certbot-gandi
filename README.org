* Certbot for gandi API
** Initial implementation

   I found this code for the entry point in the below
   ~docker-compose.yml~ snippet on the internet, that uses ~trap exit
   TERM~ to catch *SIGKILL* to terminate the script upon request. It
   also has the ~& wait $${!}~ attached to sleep for it to terminate
   immediatly and not wait for sleep to end before exiting.

 #+begin_example
 certbot:
    build: dns-gandi
    volumes:
      - "./certbot/letsencrypt:/etc/letsencrypt/"
      - "./certbot/gandi.ini:/etc/letsencrypt/gandi/gandi.ini"
    entrypoint: "/bin/sh -c 'trap exit TERM; while :; do certbot renew; sleep 12h & wait $${!}; done;'"
 #+end_example

** Podman and ~systemd~

   I moved to using a rootless podman setup and systemd for the timer
   instead of the above bash magic.

   #+begin_src shell
sudo loginctl enable-linger <certbot_user>
machinectl shell <certbot_user>@.host
mkdir -p /path/to/{log,letsencrypt,gandi}
systemctl enable --user --now podman.socket
echo "dns_gandi_api_key=<API-KEY>" > /path/to/gandi/gandi.ini
podman run --rm \
       -v /path/to/letsencrypt:/etc/letsencrypt \
       -v /path/to/gandi/gandi.ini:/etc/letsencrypt/gandi/gandi.ini \
       --entrypoint \
        '["certbot", "certonly",
        "--authenticator", "dns-gandi",
        "--dns-gandi-credentials", "/etc/letsencrypt/gandi/gandi.ini",
        "--email", "postmaster@example.com",
        "-d", "mail.example.com",
        "--rsa-key-size", "4096",
        "--agree-tos",
        "--force-renewal" ]' \
       docker.io/chrplace/certbot-gandi
podman create --name certbot-gandi \
       -v /path/to/letsencrypt:/etc/letsencrypt \
       -v /path/to/certbotlog:/var/log/letsencrypt \
       -v /path/to/gandi/gandi.ini:/etc/letsencrypt/gandi/gandi.ini \
       --entrypoint \
       '["certbot", "renew"]' \
       docker.io/chrplace/certbot-gandi
podman generate systemd --new certbot-gandi > ~/.config/systemd/user/certbot-gandi.service
cat <<EOF > ~/.config/systemd/user/certbot-gandi.timer
[Unit]
Description=Certbot daily timer

[Timer]
OnCalendar=daily
AccuracySec=12h

[Install]
WantedBy=timers.target
EOF
systemctl --user daemon-reload
systemctl --user enable --now certbot-gandi.timer
   #+end_src
